# Devops Test 01

## Requirements

- Docker
- Kubernetes
- Minikube

## Minikube configuration
```
minikube config set memory 16384
minikube config set cpus 4
kubectl create secret generic tiqets --from-literal=encrypt_key=somekey234876283
```

## Deployment of stack

```
deploy.sh
```

## Testing examples:

Application:

```
kubectl port-forward service/tiqets 5000
echo 'hello' | curl -X POST http://localhost:5000/encrypt
```

Prometheus:

```
kubectl port-forward service/prometheus 9090
```
http://localhost:9090/

Kibana:

```
kubectl port-forward service/kibana-sample-kb-http 5601
```
https://localhost:5601/
username: elastic
password:
```
kubectl get secret elasticsearch-sample-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode
```

## Removing stack

```
remove.sh
```

## Technologies

- Docker
- Kubernetes
- Prometheus
- Filebeat
- Logstash
- Elasticsearch
- Kibana
- Python, Flask

## Containerize an application

This repository contains a very simple application written in Python that encrypts and decrypts secrets.

### Instructions for running the app

```
> pip install -r requirements.txt

> FLASK_APP=app.py APP_AES_KEY=somekey234876283 flask run
```

Once it's running you can test the endpoints with

```
> echo 'hello' | curl -X POST http://localhost:5000/encrypt
```

Your goal is to improve this application to be able to run in kubernetes.

## Deliverables

- New files and modifications to the application to be able to run in Minikube (Linux) or Docker for Mac (macOS)
- Instructions on how to run the application
- BONUS: configure GitLab CI

## Notes

- Don't spend more than 2 hours on this assignment.
- Think about logging and monitoring. Suggest and implement improvement.
- Security is also our priority. What should be improved or changed? If you can, implement the changes.
- You should explain, why you made a specific technical decision.

