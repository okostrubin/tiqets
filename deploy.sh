#!/bin/sh

set -e

# Set docker env Linux
eval $(minikube docker-env)
# Set docker env Power Shell
#minikube docker-env | Invoke-Expression

docker build -t tiqets:1.0.0 ./tiqets

kubectl apply -f eck.yaml
kubectl apply -f apm_es_kibana.yaml
kubectl apply -f monitoring.yaml
kubectl apply -f logging.yaml
kubectl apply -f tiqets.yaml
