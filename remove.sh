#!/bin/sh

set -e

kubectl delete deployment tiqets
kubectl delete service tiqets
kubectl delete deployment prometheus
kubectl delete service prometheus
kubectl delete deployment apm-server-sample-apm-server
kubectl delete service elasticsearch-sample-es-http
kubectl delete service apm-server-sample-apm-http
kubectl delete deployment logstash
kubectl delete service logstash
kubectl delete deployment kibana-sample-kb
kubectl delete service kibana-sample-kb-http
