from flask import Flask, request
from Crypto.Cipher import AES
from Crypto import Random
import base64
import os
import logging

logging.basicConfig(filename='/tmp/app.log', level=logging.INFO)


log = logging.getLogger()

encrypt_key = os.getenv('APP_AES_KEY')

app = Flask(__name__)

BS = 16


def pad(s): return s + (BS - len(s) % BS) * chr(BS - len(s) % BS)


def unpad(s): return s[:-ord(s[len(s)-1:])]


def encrypt(raw):
    raw = pad(raw)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(encrypt_key, AES.MODE_CBC, iv)
    return base64.b64encode(iv + cipher.encrypt(raw))


def decrypt(enc):
    enc = base64.b64decode(enc)
    iv = enc[:16]
    cipher = AES.new(encrypt_key, AES.MODE_CBC, iv)
    return unpad(cipher.decrypt(enc[16:]))


@app.route('/encrypt', methods=['POST'])
def encrypt_route():
    log.info(f"Encrypting {request.data.decode()}")
    return encrypt(request.data.decode())


@app.route('/decrypt', methods=['POST'])
def decrypt_route():
    log.info(f"Decrypted {decrypt(request.data)}")
    return decrypt(request.data)
